import { BasicsPage } from './app.po';

describe('basics App', () => {
  let page: BasicsPage;

  beforeEach(() => {
    page = new BasicsPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
